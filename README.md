# Logging-Wrapper

## Project Name

MASYS HTW Loggingwrapper for Apache's Log4j2

## Description

A simple open-source logging wrapper for Apache's Log4j2 Logging Framework, enforcing a JSON oriented `Object | Subject | Action` logging-structure.

This was created as part of a student project at the HTW Berlin.

## Table of Contents

1. [Project Name](#project-name)
2. [Description](#description)
3. [Installation](#installation)
4. [Usage](#usage)
   1. [Root-Logger](#root-logger)
   2. [Named-Loggers](#named-loggers)
   3. [Supported log-levels](#supported-log-levels)
   4. [Configuration](#configuration)
      1. [Prior to initialization](#prior-to-initialization)
      2. [At runtime](#at-runtime)
5. [Known Issues](#known-issues)
6. [Credits](#credits)
7. [License](#license)

## Installation

Given its Maven project structure and a predefined build routine, simply call

```powershell
mvn package
```

in order to build this repository.

The `loggingwrapper\target` directory will contain at least 2 files.

- loggingWrapper.jar
- log4j2.xml

Add the .jar file to you own project by declaring a new dependency and load the .xml (or your own configuration) using one of the described configuration methods.

## Usage

### Root-Logger

Initialize the logger by creating a new wrapper instance of `com.htw.masys.loggingwrapper.logger` and log based on one of the standard log-levels.

```java
@RestController
public class SomeComponentController {

    // Init Logger similarly to Log4j2
    private final Logger LOGGER = new Logger(SomeComponentController.class);

    @PostMapping("/fatallog")
    public SomeOutput concatFatal(@RequestBody SomeInput input) {
        LOGGER.fatal("My Object", "My Subject", "My Fatal action!");
        return new SomeOutput(input);
    }
}
```

### Named-Loggers

Names loggers can be initialized the same way as you would in log4j2.

```java
    ...

    // get a named logger instance
    private final Logger LOGGER = new Logger("MyAwesomeLogger");

    ...
```

### Sample output using the default log4j2.xml (part of this repository)

```json
{
  "date": "2020-01-05 14:47:57,776",
  "log_level": "ERROR",
  "host_app": "fb31cd0e-2fc1-11ea-a602-00d8619f3c8e",
  "p_id": "9084",
  "filepath": "SomeComponentController.java",
  "context": "Main",
  "msg": {
    "object": "My Object",
    "subject": "My Subject",
    "action": "My Fatal action!"
  }
}
```

## Supported log-levels

The following log-levels are supported. Additional support for custom log-levels is currently not in place.

- FATAL - `LOGGER.fatal(object, subject, action, optionals)`
- ERROR - `LOGGER.error(object, subject, action, optionals)`
- WARN - `LOGGER.warn(object, subject, action, optionals)`
- INFO - `LOGGER.info(object, subject, action, optionals)`
- DEBUG - `LOGGER.debug(object, subject, action, optionals)`
- TRACE - `LOGGER.trace(object, subject, action, optionals)`

## Configuration

This logging wrapper allows for a variety of different configuration methods building upon Apache's versatile log4j2 configuration.

### Prior to initialization

Using Apache's Standard Log4j2 configuration method, the configuration file simply needs to be placed inside the resources directory.
By default, the most common place would be `\src\main\resources`

```txt
root:.
├───src
│   ├───main
│   │   ├───java
│   │   │   ├───controller
│   │   │   │       SomeController.java
│   │   │   │
│   │   │   └───models
│   │   │           SomeModel.java
│   │   │
│   │   └───resources
│   │           log4j2.xml // < put the config here
│   ├─ ...
├─ ...
```

Supported file-formats are

- .properties
- .yaml/.yml
- .json/jsn
- .xml

More information can be found [here](https://logging.apache.org/log4j/2.x/manual/configuration.html)

### At runtime

Runtime configuration is also supported by either referencing log4j2 directly, or by using the `configure()` method.

#### Direct reference

```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.apache.logging.log4j.core.config.Configurator;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        Configurator.initialize(null, "config/log4j2.xml"); // this uses log4j2's configurator
        SpringApplication.run(Application.class, args);
    }
}
```

#### `Configure()`

```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import loggingwrapper.Logger;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        Logger.configure("config/log4j2.xml"); // this uses Logger's configure method
        SpringApplication.run(Application.class, args);
    }
}
```

## Known issues

### Spring Boot

#### Problem

Spring boot supplies its own log4j2 configuration. Loggingwrapper will still work, it just won't use any supplied configuration - hence, the result may be different from what was intended.

#### Solution

As workaround, you can simply disable SpringBoots supplied logger profile by adding an `<exclusion>` to your `pom.xml`

```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <!-- Add this exclusion to your pom.xml to allow custom log4j2 configuration inside a SpringBoot App -->
        <exclusions>
            <exclusion>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-logging</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
```

## Credits

I would like to thank [Angelina Neuhaus](mailto:Angelina.Neuhaus@student.htw-berlin.de), [Ivan Weber](mailto:Ivan.Weber@student.htw-berlin.de), [Tim Julier](mailto:t.julier@Student.HTW-Berlin.de) and [Florian Rother](mailto:Florian.Rother@Student.HTW-Berlin.de) for contributing to this project.

## License

MIT License

Copyright (c) \[2019\] \[Erik Brüggemann\]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
