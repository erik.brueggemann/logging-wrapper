package loggingwrapper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

public class Logger {
	
	// ----------------------------------------------------------------------------------------------------------------
	// VARS -----------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------
	
	private final String EXCEPTION_MESSAGE = "Log4j2 Logger instance unset.";
	private final String FORMAT_ERROR = "Object, subject or action is null.";
	
	private final static String CONFIG_FILE_NOT_FOUND = "";
	private final static String IO_EXCEPTION_MESSAGE = "";
	
	private org.apache.logging.log4j.Logger LOGGER = null;
	
	
	// ----------------------------------------------------------------------------------------------------------------
	// CONSTRUCTORS ---------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------
	
	public Logger(final Class<?> clazz) {
		if (this.LOGGER == null) {
			this.LOGGER = LogManager.getLogger(clazz);
		}
	}
	
	public Logger(String name) {
		if (this.LOGGER == null) {
			this.LOGGER = LogManager.getLogger(name);
		}
	}
	
	/**
	 * EXPERIMENTAL!!! 
	 * Can be used to override the local configuration of the Logger at runtime.
	 * 
	 * @param pathToConfig - path/to/the/log4j2.xml|json|etc.
	 */
	public static void configure(String pathToConfig) {
		try {
			ConfigurationSource source = new ConfigurationSource(new FileInputStream(pathToConfig));
			Configurator.initialize(null, source);
		} catch (FileNotFoundException e) {
			System.out.println(CONFIG_FILE_NOT_FOUND);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(IO_EXCEPTION_MESSAGE);
			e.printStackTrace();
		}
	}
	
	
	// ----------------------------------------------------------------------------------------------------------------
	// LOGGING METHODS ------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------
	
	// FATAL ----------------------------------------------------------------------------------------------------------
	
	/**
	 * Logs object, subject and action as JSON at log-level FATAL
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 */
	public void fatal(String object, String subject, String action) {
		if (this.LOGGER != null) {
			this.LOGGER.fatal(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level FATAL
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void fatal(String object, String subject, String action, HashMap<String, String> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.fatal(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.fatal(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level FATAL.
	 * Implicitly calls each object's toString(). Unless the toString() is overridden, 
	 * the output defaults to Object's inherent toString().
	 * 
	 * @param object - object as Object
	 * @param subject - subject as Object
	 * @param action - action as Object
	 */
	public void fatal(Object object, Object subject, Object action) {
		if (this.LOGGER != null) {
			this.LOGGER.fatal(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level FATAL
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void fatal(Object object, Object subject, Object action, HashMap<Object, Object> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.fatal(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.fatal(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	
	// ERROR ----------------------------------------------------------------------------------------------------------
	
	/**
	 * Logs object, subject and action as JSON at log-level ERROR
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 */
	public void error(String object, String subject, String action) {
		if (this.LOGGER != null) {
			this.LOGGER.error(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}

	/**
	 * Logs object, subject and action as JSON at log-level ERROR
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void error(String object, String subject, String action, HashMap<String, String> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.error(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.error(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level ERROR.
	 * Implicitly calls each object's toString(). Unless the toString() is overridden, 
	 * the output defaults to Object's inherent toString().
	 * 
	 * @param object - object as Object
	 * @param subject - subject as Object
	 * @param action - action as Object
	 */
	public void error(Object object, Object subject, Object action) {
		if (this.LOGGER != null) {
			this.LOGGER.error(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level ERROR
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void error(Object object, Object subject, Object action, HashMap<Object, Object> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.error(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.error(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	
	// WARN -----------------------------------------------------------------------------------------------------------
	
	/**
	 * Logs object, subject and action as JSON at log-level WARN
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 */
	public void warn(String object, String subject, String action) {
		if (this.LOGGER != null) {
			this.LOGGER.warn(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level WARN
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void warn(String object, String subject, String action, HashMap<String, String> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.warn(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.warn(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level WARN.
	 * Implicitly calls each object's toString(). Unless the toString() is overridden, 
	 * the output defaults to Object's inherent toString().
	 * 
	 * @param object - object as Object
	 * @param subject - subject as Object
	 * @param action - action as Object
	 */
	public void warn(Object object, Object subject, Object action) {
		if (this.LOGGER != null) {
			this.LOGGER.warn(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level WARN
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void warn(Object object, Object subject, Object action, HashMap<Object, Object> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.warn(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.warn(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	// INFO -----------------------------------------------------------------------------------------------------------
	
	/**
	 * Logs object, subject and action as JSON at log-level INFO
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 */
	public void info(String object, String subject, String action) {
		if (this.LOGGER != null) {
			this.LOGGER.info(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level INFO
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void info(String object, String subject, String action, HashMap<String, String> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.info(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.info(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level INFO.
	 * Implicitly calls each object's toString(). Unless the toString() is overridden, 
	 * the output defaults to Object's inherent toString().
	 * 
	 * @param object - object as Object
	 * @param subject - subject as Object
	 * @param action - action as Object
	 */
	public void info(Object object, Object subject, Object action) {
		if (this.LOGGER != null) {
			this.LOGGER.info(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level INFO
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void info(Object object, Object subject, Object action, HashMap<Object, Object> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.info(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.info(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	
	// DEBUG ----------------------------------------------------------------------------------------------------------
	
	/**
	 * Logs object, subject and action as JSON at log-level DEBUG
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 */
	public void debug(String object, String subject, String action) {
		if (this.LOGGER != null) {
			this.LOGGER.debug(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level DEBUG
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void debug(String object, String subject, String action, HashMap<String, String> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.debug(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.debug(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level DEBUG.
	 * Implicitly calls each object's toString(). Unless the toString() is overridden, 
	 * the output defaults to Object's inherent toString().
	 * 
	 * @param object - object as Object
	 * @param subject - subject as Object
	 * @param action - action as Object
	 */
	public void debug(Object object, Object subject, Object action) {
		if (this.LOGGER != null) {
			this.LOGGER.debug(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level DEBUG
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void debug(Object object, Object subject, Object action, HashMap<Object, Object> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.debug(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.debug(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	
	// TRACE ----------------------------------------------------------------------------------------------------------
	
	/**
	 * Logs object, subject and action as JSON at log-level TRACE
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 */
	public void trace(String object, String subject, String action) {
		if (this.LOGGER != null) {
			this.LOGGER.trace(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level TRACE
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void trace(String object, String subject, String action, HashMap<String, String> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.trace(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.trace(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level TRACE.
	 * Implicitly calls each object's toString(). Unless the toString() is overridden, 
	 * the output defaults to Object's inherent toString().
	 * 
	 * @param object - object as Object
	 * @param subject - subject as Object
	 * @param action - action as Object
	 */
	public void trace(Object object, Object subject, Object action) {
		if (this.LOGGER != null) {
			this.LOGGER.trace(this.toJson(object, subject, action));
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	/**
	 * Logs object, subject and action as JSON at log-level TRACE
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @param optionals - optional params that show up in the logging entry
	 */
	public void trace(Object object, Object subject, Object action, HashMap<Object, Object> optionals) {
		if (this.LOGGER != null) {
			if (!optionals.equals(null) && optionals.size() >= 1) {
				this.LOGGER.trace(this.toJson(object, subject, action, optionals));
			} else {
				this.LOGGER.trace(this.toJson(object, subject, action));
			}
		} else {
			throw new IllegalStateException(EXCEPTION_MESSAGE);
		}
	}
	
	
	// ----------------------------------------------------------------------------------------------------------------
	// PRIVATE METHODS ------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------
	
	/**
	 * Converts all given strings into a matching JSON string.
	 * 
	 * @param object - object as String
	 * @param subject - subject as String
	 * @param action - action as String
	 * @return JSON String representing all input params.
	 */
	private String toJson(String object, String subject, String action) {
		return "{\"object\": \"" + object + "\", \"subject\": \"" + subject + "\", \"action\": \"" + action + "\"}";
	}
	
	private String toJson(String object, String subject, String action, HashMap<String, String> optionals) {
		LinkedList<String> optionalJsonBeforeConcat = new LinkedList<String>();
		
		for (Entry<String, String> entry : optionals.entrySet()) {
			optionalJsonBeforeConcat.add(convertOptionalStringToString(entry));
		}
		
		Object[] objArr = optionalJsonBeforeConcat.toArray();
		String[] strArr = new String[objArr.length];
		for(int i =0; i < objArr.length; i++) {
			strArr[i] = (String) objArr[i];
	      }
		
		
		String optionalJson = String.join(",", strArr);
		return "{\"object\": \"" + object + "\", \"subject\": \"" + subject + "\", \"action\": \"" + action + "\", " + optionalJson + "}";
	}
	
	private String convertOptionalStringToString(Entry<String, String> entry) {
		
	    String key = entry.getKey();
	    String value = entry.getValue();
		
		return " \"" + key  + "\": \"" + value + "\"";
	}
	
	
	/**
	 * Converts all given objects into a matching JSON string.
	 * If any given object is null, an empty JSON string is returned 
	 * and a format error is printed accordingly.
	 * 
	 * @param object - object as Object
	 * @param subject - subject as Object
	 * @param action - action as Object
	 * @return JSON String representing all input params.
	 */
	private String toJson(Object object, Object subject, Object action) {
		if (object != null && subject != null && action != null) {
			return "{\"object\": \"" + object.toString() + "\", \"subject\": \"" + subject.toString() + "\", \"action\": \"" + action.toString() + "\"}";
		} else {
			System.out.println(FORMAT_ERROR);
			return "{\"object\": \"\", \"subject\": \"\", \"action\": \"\"}";
		}
		
	}
	private String toJson(Object object, Object subject, Object action, HashMap<Object, Object> optionals) {
		LinkedList<String> optionalJsonBeforeConcat = new LinkedList<String>();
		
		for (Entry<Object, Object> entry : optionals.entrySet()) {
			optionalJsonBeforeConcat.add(convertOptionalOBjectToString(entry));
		}
		
		Object[] objArr = optionalJsonBeforeConcat.toArray();
		String[] strArr = new String[objArr.length];
		for(int i =0; i < objArr.length; i++) {
			strArr[i] = (String) objArr[i];
	      }
		
		
		String optionalJson = String.join(",", strArr);
		return "{\"object\": \"" + object + "\", \"subject\": \"" + subject + "\", \"action\": \"" + action + "\", " + optionalJson + "}";
	}

	private String convertOptionalOBjectToString(Entry<Object, Object> entry) {
		
	    String key = entry.getKey().toString();
	    String value = entry.getValue().toString();
		
		return "\"" + key  + "\": \"" + value + "\"";
	}
}
